# reddit.js

A simple wrapper for getting posts from Reddit. Mostly created for my own personal project that just needs basic access to post listings on subreddits.

## Usage

```js
//import the library
let { Reddit } = require("../reddit.js")

//create instance of the client
let client = new Reddit();

//Get the list of posts from r/funny, sorted by new
//Currently it is accessed using callbacks
client.get("funny", "new", function(data){
    for(let post of data){
        console.log(post);
        console.log(" ");
    }
})
/*
Example of a post:
{
  author: { name: '/u/lolnein', uri: 'https://www.reddit.com/user/lolnein' },
  category: { term: 'funny', label: 'r/funny' },
  id: 't3_ph3lh3',
  thumbnail: 'https://external-preview.redd.it/6e0p6546e7ficb1016UfYDg95kA65E9Qbtd3Z2hwAys.png?width=640&crop=smart&auto=webp&s=404dfdbf50fee5e68402125f11a03a63d199920a',
  link: 'https://www.reddit.com/r/funny/comments/ph3lh3/search_engines/',
  updated: '2021-09-03T11:49:31+00:00',
  published: '2021-09-03T11:49:31+00:00',
  title: 'Search Engines'
}
*/
```

## TODO
- [ ] Caching, prevent making multiple calls if the same subreddit is accessed
- [ ] Async functions, not just using callbacks
